package net.openesb.standalone.http;

import com.google.inject.AbstractModule;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class HttpModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(HttpServer.class).asEagerSingleton();
    }
}
